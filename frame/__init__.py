#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/4/30 10:18
# @Author  : Link
# @Site    : 
# @File    : __init__.py
# @Software: PyCharm


from .app import Main

from .interface.vue_interface import MainInterFace  # 用于挂载ViewInterface并绑定在pyqt上, 在里面编写和pyqt交互的信号

from .interface.view_interface import ViewInterface  # 用于装载功能 编写view类需要继承, 在里面编写和QWebChannel交互的接口

from .interface.channel_interface import VueElementObject  # QWebChannel Signal Object

from .utils.utils import request_vue_data, return_vue_data  # 装饰器

from .utils.utils import returnFailure, returnSuccess, returnOtherMessage
