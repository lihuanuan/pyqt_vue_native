#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : channel_interface.py
@Author  : Link
@Time    : 2021/4/17 12:11
"""

from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal

import json

from frame.logger_moudle import logger
from frame.utils.utils import returnFailure


class VueElementObject(QObject):
    func_dict = {}
    connectSignal = pyqtSignal(str)  # QWebChannel Signal

    # @call_back_vue  # 不能加上装饰 装饰器加上了后 在前端不会有request Object
    @pyqtSlot(str)
    def request(self, req: str):
        logger.info("vue request: {}".format(req))
        try:
            request_data = json.loads(req)
            func = request_data.get("func", None)
            if func is None:
                raise Exception("没有传入[func]函数名")
            qt_func = self.func_dict.get(func, None)
            if qt_func is None:
                raise Exception("QT中不存在此函数:{}用于调用".format(func))
            data = qt_func(request_data)
            # 如果返回了数据, 就通过信号返回给前端 否则结束流程
            if data is None:
                return
            if not isinstance(data, str):
                raise Exception("返回值必须是一个json字符串类型")
            logger.info("pyqt emit: {}".format(data))
            self.connectSignal.emit(data)
        except Exception as err:
            logger.warning("pyqt error: {}".format(err))
            self.connectSignal.emit(returnFailure(str(err)))
