#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/4/28 13:14
# @Author  : Link
# @Site    : 
# @File    : view_interface.py
# @Software: PyCharm
from frame.utils.utils import returnOtherMessage, returnFailure
from .channel_interface import VueElementObject


class ViewInterface:

    def __init__(self, vue_obj: VueElementObject):
        self.vue_obj = vue_obj
        self.func_init()

    def func_init(self):
        for each in filter(lambda m: not m.startswith("_") and
                                     not m.endswith("_") and
                                     callable(getattr(self, m)), dir(self)):  # callable 表示为可以被调用执行的对象
            self.vue_obj.func_dict[each] = getattr(self, each)

    def set_fail_message(self, message: str):
        self.vue_obj.connectSignal.emit(returnFailure(message))

    def set_other_message(self, message: str, code: int):
        self.vue_obj.connectSignal.emit(returnOtherMessage(message, code))
