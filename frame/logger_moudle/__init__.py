#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/4/23 10:13
# @Author  : Link
# @Site    : 
# @File    : __init__.py.py
# @Software: PyCharm

from loguru import logger

logger_config = logger.add('logger.log', level="INFO")
