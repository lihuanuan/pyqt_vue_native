#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2021/4/28 17:35
# @Author  : Link
# @Site    : 
# @File    : table.py
# @Software: PyCharm
from faker import Faker

fake = Faker()


def generator_table(row: int):
    data = []
    for i in range(row):
        data.append({
            'id': i,
            'title': fake.text(),
            'author': fake.name(),
            'display_time': fake.date_time().strftime("%Y-%m-%d %H:%M:%S"),
            'pageviews': fake.random_int(),
            'status': fake.random_sample(['published', 'draft', 'deleted'], 1)[0]
        })
    return data