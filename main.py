#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : main.py
@Author  : Link
@Time    : 2021/4/17 9:49
"""

import sys

from PyQt5.QtWebChannel import QWebChannel
from PyQt5.QtWidgets import QApplication

from frame import Main  # 需要传入的参数 一个QObject和一个QWebChannel
from frame import MainInterFace  # QObject给QWebChannel发送信号

from custom.view.table import A  # 测试
from custom.view.liv_view import TO_LIV_KEY


# 遇到打包问题参照 https://blog.csdn.net/wuwei_201/article/details/104595963
class MyMainInterFace(MainInterFace):

    def __init__(self):
        super(MyMainInterFace, self).__init__()
        TO_LIV_KEY(self)
        A(self)


myObj = MyMainInterFace()
channel = QWebChannel()
channel.registerObject("bridge", myObj)


class Application(QApplication):
    def __init__(self, argv):
        QApplication.__init__(self, argv)
        QApplication.setStyle('Fusion')


if __name__ == '__main__':
    app = Application(sys.argv)
    myWin = Main(myObj, channel)
    myWin.show()
    sys.exit(app.exec_())
